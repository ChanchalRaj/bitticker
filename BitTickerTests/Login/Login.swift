//
//  Login.swift
//  BitTickerTests
//
//  Created by Raj on 14/06/2020.
//

import XCTest

@testable import BitTicker

class Login: XCTestCase {
    
    var moduleViewController: LoginViewInterface!
    var wireframe: LoginWireframeInterface!
    var interactor:LoginInteractorInterface!
    var presenter: LoginPresenterInterface!

    let mockUserLoginNetworkDAO = MockUserLoginNetworkDAO()
    override func setUp(){
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        moduleViewController = MockLoginViewController()
        wireframe = MockLoginWireframe()
        interactor = LoginInteractor(networkDao: mockUserLoginNetworkDAO,
                                         keychainDao: UserKeychainDAO(keychain: Keychain()))
        presenter = LoginPresenter(view: moduleViewController,
                                       interactor: interactor,
                                       wireframe: LoginWireframe())
    }
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testValidLogin() {
        defer {
            let keychain = UserKeychainDAO.init(keychain: Keychain.init())
            _ = keychain.unsaveUser()
        }
        let mockUser = User(id: 15, email: "chanchalraj@gmail.com",
        name: "Chanchal Raj",
        authenticationToken: "123123123å")
        let respone = LoginAPIResponse(messages: "",
        isSuccess: true,
        data: DataClass(user: mockUser))
        mockUserLoginNetworkDAO.fakeResult = .success(respone)
        presenter.login(email: "chanchalraj@gmail.com", password: "apple123")
        let userKeychainDAO = UserKeychainDAO.init(keychain:Keychain())
        guard let savedUser = userKeychainDAO.getUser() else { return }
        
        XCTAssertEqual(savedUser, mockUser, "User could not be saved successfully")
    }
    
    func testInvalidLogin() {
        
        defer {
            let keychain = UserKeychainDAO.init(keychain: Keychain.init())
            _ = keychain.unsaveUser()
        }
        mockUserLoginNetworkDAO.fakeResult = .failure(NetworkError.unAuthorized("Invalid email or password"))
        presenter.login(email: "chanchalraj@gmail.com", password: "apple123")
        let userKeychainDAO = UserKeychainDAO.init(keychain:Keychain())
        let savedUser = userKeychainDAO.getUser()
        
        XCTAssertEqual(savedUser, nil, "User saved, when it shouldn't be saved in keychain")
    }
}

//
//  MockLoginWireframe.swift
//  BitTickerTests
//
//  Created by Raj on 14/06/2020.
//

import Foundation
import UIKit

@testable import BitTicker

final class MockLoginWireframe: LoginWireframeInterface {
    func openRegisterModule() {
        ///
    }
    
    func openTradeTickerModule() {
        ///
    }
    
    func showAlert(title: String, message: String, style: UIAlertController.Style, preferredActionIndex: Int?, actions: UIAlertAction...) {
        ///
    }
    
    func showAlert(title: String, message: String, cancelButtonTitle: String) {
        ///
    }
}

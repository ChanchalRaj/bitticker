//
//  MockUserLoginNetworkDAO.swift
//  BitTickerTests
//
//  Created by Raj on 14/06/2020.
//

import Foundation
@testable import BitTicker

class MockUserLoginNetworkDAO: LoginNetworkDAOProtocol {
    
    //server result
    //it can be anything
    //dictionrar, empty, false, true etc etc
    var fakeResult: Result<LoginAPIResponse,Error>!
    
        
    func login(email: String, password: String,
               completion: @escaping (Result<LoginAPIResponse, Error>) -> Void) {
        completion(fakeResult)
    }
}

//
//  MockLoginViewController.swift
//  BitTickerTests
//
//  Created by Raj on 14/06/2020.
//

import Foundation
@testable import BitTicker

class MockLoginViewController: LoginViewInterface {
    
    var presenter: LoginPresenterInterface!

    func showMessage(message: String) {
    }
    func didLoginSuccessfully(){
    }
    func shakeEmailField() {
    }
    func shakePasswordField() {
    }
    func hideKeyboard(){
    }
    func showLoader() {
    }
    func hideLoader() {
    }
}

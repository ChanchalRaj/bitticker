import UIKit

protocol WireframeInterface: class {
    func showAlert(title:String,message:String,style:UIAlertController.Style,preferredActionIndex:Int?,actions:UIAlertAction...)
    func showAlert(title:String,message:String,cancelButtonTitle:String)
}

class BaseWireframe {

    private unowned var _viewController: UIViewController
    
    //to retain view controller reference upon first access
    private var _temporaryStoredViewController: UIViewController?

    init(viewController: UIViewController) {
        _temporaryStoredViewController = viewController
        _viewController = viewController
    }

}

extension BaseWireframe: WireframeInterface {
    
}

extension BaseWireframe {
    
    var viewController: UIViewController {
        defer { _temporaryStoredViewController = nil }
        return _viewController
    }
    
    var navigationController: UINavigationController? {
        return viewController.navigationController
    }
    
}

extension UIViewController {
    
    func presentWireframe(_ wireframe: BaseWireframe, animated: Bool = true, completion: (() -> Void)? = nil) {
        present(wireframe.viewController, animated: animated, completion: completion)
    }
    
}

extension UINavigationController {
    
    func pushWireframe(_ wireframe: BaseWireframe, animated: Bool = true) {
        self.pushViewController(wireframe.viewController, animated: animated)
    }
    
    func setRootWireframe(_ wireframe: BaseWireframe, animated: Bool = true) {
        self.setViewControllers([wireframe.viewController], animated: animated)
    }
}
extension BaseWireframe{
    /**
     Shows UIAlertController with given parameters
     - Parameter viewController: The UIViewController which the UIAlertController will be shown on
     - Parameter title: The title of the UIAlertController
     - Parameter message: The message of the UIAlertController
     - Parameter style: The style of the UIAlertController i.e. either alert or actionsheet. **alert** style is set as default.
     - Parameter actions: The list of actions to be added on UIAlertController
     */
    func showAlert(title:String,message:String,style:UIAlertController.Style = .alert,preferredActionIndex:Int? = nil,actions:UIAlertAction...){
        DispatchQueue.main.async {[weak self] in
            let alert = UIAlertController.init(title: title, message: message, preferredStyle: style)
            for action in actions{
                alert.addAction(action)
            }
            if let preferredActionIndex = preferredActionIndex, preferredActionIndex < actions.count{
                let preferredAction = actions[preferredActionIndex]
                alert.preferredAction = preferredAction
            }
            self?.viewController.present(alert, animated: true, completion: nil)
        }
    }
    func showAlert(title:String,message:String,cancelButtonTitle:String = "OK"){
        let action = UIAlertAction.init(title: cancelButtonTitle, style: .default, handler: nil)
        self.showAlert(title: title, message: message, actions: action)
    }
}


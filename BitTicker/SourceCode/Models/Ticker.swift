//
//  Ticker.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation
struct TickerResponse: Decodable {
    let id: Int32?
    let nilVal: String?
    let ticker: Ticker?
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        id = try container.decode(Int32.self)
        nilVal = try container.decode(String?.self)
        ticker = try container.decode(Ticker.self)
    }
}

struct Ticker: Decodable {
    let currencyPairId: Int
    let lastTradePrice: Double
    let lowestAsk: Double
    let highestBid: Double
    let percentChange: Double
    let baseCurrencyVolume: Double
    let quoteCurrencyVolume: Double
    let isFrozen: Bool
    let highestTradePrice: Double
    let lowestTradePrice: Double
    var currencyPairName:String?{
        return CurrencyPair.init(rawValue: currencyPairId)?.description()
    }
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        currencyPairId = try container.decode(Int.self)
        //lastTradePrice = try container.decode(String.self)
        guard let price = Double(try container.decode(String.self)),
        let lAsk = Double(try container.decode(String.self)),
        let hBid = Double(try container.decode(String.self)),
        let pChange = Double(try container.decode(String.self)),
        let bCurrencyVolume = Double(try container.decode(String.self)),
        let qCurrencyVolume = Double(try container.decode(String.self)),
        let iFrozen = try container.decode(Int.self) == 1 ? true : false,
        let hTradePrice = Double(try container.decode(String.self)),
        let lTradePrice = Double(try container.decode(String.self))
        else{
            throw CustomError.generalError()
        }
        
        lastTradePrice = price
        lowestAsk = lAsk
        highestBid = hBid
        percentChange = pChange
        baseCurrencyVolume = bCurrencyVolume
        quoteCurrencyVolume = qCurrencyVolume
        isFrozen = iFrozen
        highestTradePrice = hTradePrice
        lowestTradePrice = lTradePrice
    }
}
//struct Ticker{
//    let currencyPairId:Int
//    let lastTradePrice:Double
//    let lowestAsk:Double
//    let highestBid:Double
//    let percentChangeInLast24Hours:Double
//    let baseCurrencyVolumeInLast24Hours:Double
//    let quoteCurrencyVolumeInLast24Hours:Double
//    let isFrozen:Bool
//    let highestTradePriceInLast24Hours:Double
//    let lowestTradePriceInLast24Hours:Double
//    init?(values:[Any]) {
//        //Make sure API returns total 9 values in the array to comply with the ticker API documentation
//        //Otherwise failable initializer will fail to initialize
//        guard values.count == 10 else {
//            return nil
//        }
//        var currencyPairId:Int?
//        var lastTradePrice:Double?
//        var lowestAsk:Double?
//        var highestBid:Double?
//        var percentChangeInLast24Hours:Double?
//        var baseCurrencyVolumeInLast24Hours:Double?
//        var quoteCurrencyVolumeInLast24Hours:Double?
//        var isFrozen:Bool?
//        var highestTradePriceInLast24Hours:Double?
//        var lowestTradePriceInLast24Hours:Double?
//
//        for (index,coinTicker) in values.enumerated(){
//            if index == 0, let id = coinTicker as? Int{
//                currencyPairId = id
//            }else if index == 1, let lastTradePriceString = coinTicker as? String, let lastTradePriceLocal = Double(lastTradePriceString){
//                lastTradePrice = lastTradePriceLocal
//            }else if index == 2, let lowestAskString = coinTicker as? String, let lowestAskLocal = Double(lowestAskString){
//                lowestAsk = lowestAskLocal
//            }else if index == 3, let highestBidString = coinTicker as? String, let highestBidLocal = Double(highestBidString){
//                highestBid = highestBidLocal
//            }else if index == 4, let percentChangeInLast24HoursString = coinTicker as? String, let percentChangeInLast24HoursLocal = Double(percentChangeInLast24HoursString){
//                percentChangeInLast24Hours = percentChangeInLast24HoursLocal
//            }else if index == 5, let baseCurrencyVolumeInLast24HoursString = coinTicker as? String, let baseCurrencyVolumeInLast24HoursLocal = Double(baseCurrencyVolumeInLast24HoursString){
//                baseCurrencyVolumeInLast24Hours = baseCurrencyVolumeInLast24HoursLocal
//            }else if index == 6, let quoteCurrencyVolumeInLast24HoursString = coinTicker as? String, let quoteCurrencyVolumeInLast24HoursLocal = Double(quoteCurrencyVolumeInLast24HoursString){
//                quoteCurrencyVolumeInLast24Hours = quoteCurrencyVolumeInLast24HoursLocal
//            }else if index == 7, let isFrozenInt = coinTicker as? Int, let isFrozenLocal = isFrozenInt == 1 ? true : false{
//                isFrozen = isFrozenLocal
//            }else if index == 8, let highestTradePriceInLast24HoursString = coinTicker as? String, let highestTradePriceInLast24HoursLocal = Double(highestTradePriceInLast24HoursString){
//                highestTradePriceInLast24Hours = highestTradePriceInLast24HoursLocal
//            }else if index == 9, let lowestTradePriceInLast24HoursString = coinTicker as? String, let lowestTradePriceInLast24HoursLocal = Double(lowestTradePriceInLast24HoursString){
//                lowestTradePriceInLast24Hours = lowestTradePriceInLast24HoursLocal
//            }
//        }
//        if let currencyPairId = currencyPairId,
//            let lastTradePrice = lastTradePrice,
//            let lowestAsk = lowestAsk,
//            let highestBid = highestBid,
//            let percentChangeInLast24Hours = percentChangeInLast24Hours,
//            let baseCurrencyVolumeInLast24Hours = baseCurrencyVolumeInLast24Hours,
//            let quoteCurrencyVolumeInLast24Hours = quoteCurrencyVolumeInLast24Hours,
//            let isFrozen = isFrozen,
//            let highestTradePriceInLast24Hours = highestTradePriceInLast24Hours,
//            let lowestTradePriceInLast24Hours = lowestTradePriceInLast24Hours{
//
//            self.currencyPairId = currencyPairId
//            self.lastTradePrice = lastTradePrice
//            self.lowestAsk = lowestAsk
//            self.highestBid = highestBid
//            self.percentChangeInLast24Hours = percentChangeInLast24Hours
//            self.baseCurrencyVolumeInLast24Hours = baseCurrencyVolumeInLast24Hours
//            self.quoteCurrencyVolumeInLast24Hours = quoteCurrencyVolumeInLast24Hours
//            self.isFrozen = isFrozen
//            self.highestTradePriceInLast24Hours = highestTradePriceInLast24Hours
//            self.lowestTradePriceInLast24Hours = lowestTradePriceInLast24Hours
//        }else{
//            return nil
//        }
//    }
//}

//
//  LoginAPIRequestParams.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation
// MARK: - LoginAPIRequestParam
struct LoginAPIRequestParam: Codable {
    let signIn: SignIn
    enum CodingKeys: String, CodingKey {
        case signIn = "sign_in"
    }
    init(email:String,password:String) {
        let signIn = SignIn.init(email: email, password: password)
        self.signIn = signIn
    }
}

// MARK: - SignIn
struct SignIn: Codable {
    let email, password: String
}

//
//  LoginAPIResponse.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation

// MARK: - LoginAPIResponse
struct LoginAPIResponse: Codable {
    let messages: String
    let isSuccess: Bool
    let data: DataClass?

    enum CodingKeys: String, CodingKey {
        case messages
        case isSuccess = "is_success"
        case data
    }
}
// MARK: - DataClass
struct DataClass: Codable {
    let user: User?
}

// MARK: - User
struct User: Codable,Equatable {
    let id: Int
    let email, name: String
    let authenticationToken: String

    enum CodingKeys: String, CodingKey {
        case id, email, name
        case authenticationToken = "authentication_token"
    }
}

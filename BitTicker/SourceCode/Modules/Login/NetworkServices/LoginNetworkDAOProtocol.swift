//
//  LoginNetworkDAOProtocol.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation
protocol LoginNetworkDAOProtocol {
    func login(email: String, password: String,completion:@escaping(Result<LoginAPIResponse,Error>)->Void)
}

//
//  UserLoginNetworkDAO.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation

class UserLoginNetworkDAO: LoginNetworkDAOProtocol {
    func login(email: String, password: String,completion:@escaping(Result<LoginAPIResponse,Error>)->Void){
        let router = APIRouter.login(params: LoginAPIRequestParam.init(email: email, password: password))
        guard let request = router.request() else {return}
        APIClient().performRequest(request: request, completion: completion)
    }
}

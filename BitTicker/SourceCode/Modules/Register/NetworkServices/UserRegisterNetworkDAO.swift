//
//  UserRegisterNetworkDAO.swift
//  BitTicker
//
//  Created by Raj on 6/13/20.
//

import Foundation
public class UserRegisterNetworkDAO:RegisterNetworkDAOProtocol {
    func register(name: String, email: String, password: String, completion: @escaping (Result<LoginAPIResponse, Error>) -> Void) {
        let router = APIRouter.register(params: RegisterAPIRequestParam.init(name:name, email: email, password: password))
        guard let request = router.request() else {return}
        APIClient().performRequest(request: request, completion: completion)
    }
}

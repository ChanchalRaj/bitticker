//
//  RegisterAPIRequestParam.swift
//  BitTicker
//
//  Created by Raj on 6/13/20.
//

import Foundation
// MARK: - LoginAPIRequestParam
struct RegisterAPIRequestParam: Codable {
    let user: UserRegister
    init(name:String,email:String,password:String) {
        let user = UserRegister.init(name:name, email: email, password: password)
        self.user = user
    }
}

// MARK: - UserRegister
struct UserRegister: Codable {
    let name, email, password: String
}

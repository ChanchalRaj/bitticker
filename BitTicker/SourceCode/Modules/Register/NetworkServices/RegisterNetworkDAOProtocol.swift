//
//  RegisterNetworkDAOProtocol.swift
//  BitTicker
//
//  Created by Raj on 6/13/20.
//

import Foundation
protocol RegisterNetworkDAOProtocol {
    func register(name: String,email: String, password: String,completion:@escaping(Result<LoginAPIResponse,Error>)->Void)
}

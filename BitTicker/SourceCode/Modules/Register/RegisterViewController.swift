//
//  RegisterViewController.swift
//  BitTicker
//
//  Created by Raj on 6/9/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class RegisterViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    
    
    // MARK: - Public properties -

    var presenter: RegisterPresenterInterface!

    // MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()
    }

    /**
         Initializes UI components
     */
    private func layoutUI(){
        navigationController?.setNavigationBarHidden(true, animated: false)
        view.backgroundColor = Theme.Common.backgroundColor
        buttonRegister.rounded()
    }
    //MARK: - IBActions
    @IBAction func buttonBackTapped(_ sender: Any) {
        presenter.popToLoginModule()
    }
    @IBAction func buttonRegisterTapped(_ sender: Any) {
        presenter.register(name: textName.text, email: textEmail.text, password: textPassword.text)
    }
    
    // MARK: - Helper Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideKeyboard()
    }
}

// MARK: - Extensions -

extension RegisterViewController: RegisterViewInterface,Loadable {
    func showLoader() {
        DispatchQueue.main.async {[weak self] in
            self?.buttonRegister.isEnabled = false
            self?.showLoadingView()
        }
        
    }
    func hideLoader() {
        DispatchQueue.main.async {[weak self] in
            self?.buttonRegister.isEnabled = true
            self?.hideLoadingView()
        }
    }
    func didRegisterSuccessfully(){
        DispatchQueue.main.async {[weak self] in
            self?.presenter.openTradeTickerModule()
        }
    }
    func shakeNameField() {
        textName.shake()
    }
    func shakeEmailField() {
        textEmail.shake()
    }
    func shakePasswordField() {
        textPassword.shake()
    }
    func hideKeyboard(){
        view.endEditing(true)
    }
}

//
//  TradeTickerWireframe.swift
//  BitTicker
//
//  Created by Raj on 6/9/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class TradeTickerWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "TradeTicker", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: TradeTickerViewController.self)
        super.init(viewController: moduleViewController)
        guard let socketTradingURL = URL.init(string: API.Trading.baseURL) else {
            assert(true,"Invalid Trade Socket URL.")
            return
        }
        let socketManager = SocketManager.init(url: socketTradingURL)
        let interactor = TradeTickerInteractor.init(socketManager: socketManager)
        let presenter = TradeTickerPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension TradeTickerWireframe: TradeTickerWireframeInterface {
    func openLoginModule() {
        let loginModule = LoginWireframe.init()
        let navigationController = UINavigationController.init(rootViewController: loginModule.viewController)
        if let window = viewController.view.window{
            window.rootViewController = navigationController
        }
    }
}

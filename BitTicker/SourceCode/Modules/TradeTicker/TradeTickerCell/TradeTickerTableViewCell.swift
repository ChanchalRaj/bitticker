//
//  TradeTickerTableViewCell.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import UIKit

class TradeTickerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTest: UILabel!
    @IBOutlet weak var imageArrow: UIImageView!
    @IBOutlet weak var labelLastTradePrice: UILabel!
    @IBOutlet weak var labelPercentageChange: UILabel!
    var viewModel:TradeTickerTableViewCellViewModel?{
        didSet{
            if let viewModel = viewModel{
                lblTest.text = viewModel.value()
                if viewModel.isValuePositive(){
                    imageArrow.tintColor = .green
                    imageArrow.image = UIImage.init(systemName: "arrow.up")
                }else{
                    imageArrow.tintColor = .red
                    imageArrow.image = UIImage.init(systemName: "arrow.down")
                }
                labelLastTradePrice.text = viewModel.lastTradePrice()
                labelPercentageChange.text = viewModel.percentageChange()
            }
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

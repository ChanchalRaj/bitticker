//
//  TradeTickerTableViewCellViewModel.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation
struct TradeTickerTableViewCellViewModel {
    private var ticker:Ticker
    init(ticker:Ticker) {
        self.ticker = ticker
    }
    
    func value() -> String{
        return ticker.currencyPairName ?? ""
    }
    func isValuePositive() -> Bool{
        return ticker.percentChange > 0
    }
    func lastTradePrice()->String{
        return "\(ticker.lastTradePrice)"
    }
    func percentageChange()->String{
        return "\(ticker.percentChange)%"
    }
}

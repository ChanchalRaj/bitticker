//
//  FormValidator.swift
//  BitTicker
//
//  Created by Raj on 6/13/20.
//

import Foundation
struct FormValidator {
    static func validateEmail(email:String?) -> Bool{
        let email = email?.trimmingCharacters(in: .whitespaces)
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    static func validatePassword(password: String?) -> Bool {
        guard let password = password else{return false}
        let trimmedPassword = password.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedPassword.count >= 6
    }
    static func validateName(name:String?)->Bool{
        guard let name = name else{return false}
        let trimmedName = name.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedName.count > 0
    }
}

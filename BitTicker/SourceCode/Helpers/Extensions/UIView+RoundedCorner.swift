//
//  UIView+RoundedCorner.swift
//  BitTicker
//
//  Created by Raj on 6/9/20.
//

import Foundation
import UIKit

extension UIView{
    func rounded(cornerRadius:CGFloat = 5.0){
        layer.cornerRadius = cornerRadius
    }
}

//
//  Encodable+Dictionary.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation
extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}
extension Encodable {
    func toJSONData() -> Data?{ try? JSONEncoder().encode(self) }
}

//
//  UIAlertController+Extension.swift
//  BitTicker
//
//  Created by Raj on 6/13/20.
//

import Foundation
import UIKit

extension UIAlertController{
    /**
     Shows UIAlertController with given parameters
     - Parameter viewController: The UIViewController which the UIAlertController will be shown on
     - Parameter title: The title of the UIAlertController
     - Parameter message: The message of the UIAlertController
     - Parameter style: The style of the UIAlertController i.e. either alert or actionsheet. **alert** style is set as default.
     - Parameter actions: The list of actions to be added on UIAlertController
     */
    class func showAlert(in viewController:UIViewController,title:String,message:String,style:UIAlertController.Style = .alert,preferredActionIndex:Int? = nil,actions:UIAlertAction...){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: style)
        for action in actions{
            alert.addAction(action)
        }
        if let preferredActionIndex = preferredActionIndex, preferredActionIndex < actions.count{
            let preferredAction = actions[preferredActionIndex]
            alert.preferredAction = preferredAction
        }
        viewController.present(alert, animated: true, completion: nil)
    }
    class func showAlert(in viewController:UIViewController,title:String,message:String,cancelButtonTitle:String = "OK"){
        let action = UIAlertAction.init(title: cancelButtonTitle, style: .default, handler: nil)
        self.showAlert(in: viewController, title: title, message: message, actions: action)
    }
}


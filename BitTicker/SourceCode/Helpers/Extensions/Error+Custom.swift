//
//  Error+Custom.swift
//  BitTicker
//
//  Created by Raj on 6/14/20.
//

import Foundation
enum CustomError{
    case error(String)
    case detailedMessage(String,String)
    case generalError(String = "There is something wrong.")
}
extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .error(let message),.generalError(let message):
            return NSLocalizedString(message, comment: "")
        case .detailedMessage( _, let message):
            return NSLocalizedString(message, comment: "")
        }
    }
}

//
//  Encodable+String.swift
//  BitTicker
//
//  Created by Raj on 6/10/20.
//

import Foundation
extension Encodable{
    func encodeToString()->String?{
        do {
            let encoder = JSONEncoder()
            let jsonData = try encoder.encode(self)
            guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
                return nil
            }
            return jsonString
        } catch _ {
            return nil
        }
    }
}

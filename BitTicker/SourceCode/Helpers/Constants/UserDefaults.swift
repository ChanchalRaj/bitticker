//
//  UserDefaults.swift
//  BitTicker
//
//  Created by Raj on 6/13/20.
//

import Foundation

struct Defaults{
    static let id = "id"
    static let name = "name"
    static let email = "email"
    static let authToken = "authToken"
}

//
//  API.swift
//  BitTicker
//
//  Created by Raj on 6/9/20.
//

import Foundation

struct API{
    struct Trading{
        static let baseURL = "wss://api2.poloniex.com"
    }
    struct BitTicker{
        static let sandbox = "https://shrouded-journey-41838.herokuapp.com/api/"
        static let production = "https://shrouded-journey-41838.herokuapp.com/api/"
    }
}

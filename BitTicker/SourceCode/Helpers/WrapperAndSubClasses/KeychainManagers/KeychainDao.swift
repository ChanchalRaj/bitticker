//
//  KeychainDao.swift
//  BitTicker
//
//  Created by Raj on 6/13/20.
//

import Foundation
protocol UserKeychainDAOProtocol {
    func saveUser(user: User)->Bool
    func getUser()->User?
    func isUserSessionValid()->Bool
    func unsaveUser()->Bool
}

struct UserKeychainDAO:UserKeychainDAOProtocol {
    func isUserSessionValid() -> Bool {
        return getUser() != nil
    }
    
    let keychain: Keychain
    init(keychain:Keychain) {
        self.keychain = keychain
    }
    func saveUser(user: User) -> Bool {
        guard let id = "\(user.id)".data(using: .utf8),
            let email = user.email.data(using: .utf8),
            let authToken = user.authenticationToken.data(using: .utf8),
            let name = user.name.data(using: .utf8) else {
                return false
        }
        do {
            try keychain.set(value: id, account: Defaults.id)
            try keychain.set(value: email, account: Defaults.email)
            try keychain.set(value: authToken, account: Defaults.authToken)
            try keychain.set(value: name, account: Defaults.name)
        } catch {
            return false
        }
        
        return true
    }
    func getUser() -> User? {
        do {
            guard let idData = try keychain.get(account: Defaults.id), let idString = String.init(data: idData, encoding: .utf8), let id = Int(idString)else{
                return nil
            }
            guard let emailData = try keychain.get(account: Defaults.email), let email = String.init(data: emailData, encoding: .utf8) else {
                return nil
            }
            guard let nameData = try keychain.get(account: Defaults.name), let name = String.init(data: nameData, encoding: .utf8) else {
                return nil
            }
            guard let authTokenData = try keychain.get(account: Defaults.authToken), let authToken = String.init(data: authTokenData, encoding: .utf8) else {
                return nil
            }
            return User.init(id: id, email: email, name: name, authenticationToken: authToken)
        } catch {
            return nil
        }
    }
    func unsaveUser() -> Bool {
        do {
            try keychain.deleteAll()
            return true
        } catch {
            return false
        }
    }
}


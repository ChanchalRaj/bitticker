//
//  APIRouter.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation

enum Enviroment{
    case sandbox
    case production
    
    var baseURL:String{
        switch self{
        case .sandbox:
            return API.BitTicker.sandbox
        case .production:
            return API.BitTicker.production
        }
    }
}
enum APIRouter {
    case login(params:LoginAPIRequestParam)
    case register(params:RegisterAPIRequestParam)

    //case getDiaries
    // MARK: - HTTPMethod
    static var environment:Enviroment{
        #if DEBUG
        return .sandbox
        #endif
    }
    var apiVersion:String{
        switch self {
        default:
            return "v1/"
        }
    }
    var method: HTTPMethod {
        switch self {
        case .login, .register:
            return .post
        }
    }
    
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .login:
            return "sign_in.json"
        case .register:
            return "sign_up.json"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Encodable? {
        switch self {
        case .login(let params):
            return params
        case .register(let params):
            return params
        }
    }
    
    // MARK: - URLRequestConvertible
    private func asURLRequest() throws -> URLRequest {
        let url = try (APIRouter.environment.baseURL + apiVersion).asURL().appendingPathComponent(path)
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = ["Content-Type":"application/json"]
        request.timeoutInterval = TimeInterval(30)
        if let parameters = self.parameters,let paramData = parameters.toJSONData(){
            request.httpBody = paramData
        }
        return request
    }
    func request()->URLRequest?{
        guard let request = try? asURLRequest() else {
            return nil
        }
        return request
    }
}


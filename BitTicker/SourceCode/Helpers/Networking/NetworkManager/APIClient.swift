//
//  APIClient.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation
struct APIClient {
    
    func performRequest<T:Decodable>(request:URLRequest,completion:@escaping (Result<T,Error>)->Void){
        //let task = URLSession.shared.dataTask(with: request)
        let task = URLSession.shared.dataTask(with: request) {(data, urlResponse, error) in
            if let error = self.validate(response: urlResponse){
                completion(.failure(error))
            }else{
                guard let data = data else {return}
                    do{
                         let responseModel = try JSONDecoder().decode(T.self, from: data)
                        completion(.success(responseModel))
                     }catch let error{
                        completion(.failure(error))
                    }
            }
        }
        task.resume()
    }
    private func validate(response:URLResponse?,validRange:Range<Int> = 200..<401)->NetworkError?{
        if let response = response as? HTTPURLResponse{
            let statusCode = response.statusCode
            return NetworkError.error(statusCode: statusCode, validRange: validRange)
        }else{
            return nil
        }
    }
}

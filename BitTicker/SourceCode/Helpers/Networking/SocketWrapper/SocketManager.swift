//
//  SocketManager.swift
//  BitTicker
//
//  Created by Raj on 6/12/20.
//

import Foundation

protocol SocketConnection {
    func send(text: String)
    func send(data: Data)
    func connect()
    func disconnect()
    var delegate: SocketManagerDelegate? {
        get
        set
    }
}

protocol SocketManagerDelegate {
    func onConnected(connection: SocketConnection)
    func onDisconnected(connection: SocketConnection, error: Error?)
    func onError(connection: SocketConnection, error: Error)
    func onMessage(connection: SocketConnection, text: String)
    func onMessage(connection: SocketConnection, data: Data)
}

class SocketManager: NSObject, SocketConnection, URLSessionWebSocketDelegate {
    var delegate: SocketManagerDelegate?
    var webSocketTask: URLSessionWebSocketTask!
    var urlSession: URLSession!
    let delegateQueue = OperationQueue()
    
    init(url: URL) {
        super.init()
        urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: delegateQueue)
        webSocketTask = urlSession.webSocketTask(with: url)
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        self.delegate?.onConnected(connection: self)
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        self.delegate?.onDisconnected(connection: self, error: nil)
    }
    /**
               Connects to given server URL
    */
    func connect() {
        webSocketTask.resume()
        
        listen()
    }
    
    /**
                Disconnects the connection to WebSocket Server
     */
    func disconnect() {
        webSocketTask.cancel(with: .goingAway, reason: nil)
    }
    /**
            Listens updates from WebSocket server
     */
    func listen()  {
        webSocketTask.receive { result in
            switch result {
            case .failure(let error):
                self.delegate?.onError(connection: self, error: error)
            case .success(let message):
                switch message {
                case .string(let text):
                    self.delegate?.onMessage(connection: self, text: text)
                case .data(let data):
                    self.delegate?.onMessage(connection: self, data: data)
                @unknown default:
                    fatalError()
                }
                //Calling listen is recurisvely because WebSockets interface was very recently introduced and doesn't have a 'subscribe' method to listen server triggers unlike previous C-based APIs
                //The reason to avoid using third-party solutions like 'Startscream' was to use native-solutions as much as possible.
                self.listen()
            }
        }
    }
    
    func send(text: String) {
        webSocketTask.send(URLSessionWebSocketTask.Message.string(text)) { error in
            if let error = error {
                self.delegate?.onError(connection: self, error: error)
            }
        }
    }
    
    func send(data: Data) {
        webSocketTask.send(URLSessionWebSocketTask.Message.data(data)) { error in
            if let error = error {
                self.delegate?.onError(connection: self, error: error)
            }
        }
    }
}

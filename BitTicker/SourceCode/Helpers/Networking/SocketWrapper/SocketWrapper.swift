//
//  SocketWrapper.swift
//  BitTicker
//
//  Created by Raj on 6/9/20.
//

import Foundation
import Network
//"`wss://api2.poloniex.com`"
protocol SocketWrapperDelegate{
    func socketDidConnect()
    func socketDidDisconnect()
    func socketConnectionFailed()
}
typealias SocketSubscriptionResponse = (Result<Bool,Error>)->Void
typealias OpenConnectionResponse = (Bool)->Void
class SocketWrapper:NSObject{
    private let url:URL
    private var webSocketTask:URLSessionWebSocketTask?
    private var delegate:SocketWrapperDelegate?
    private var connectionDidOpen:OpenConnectionResponse?
    private var subscriptionResponse:SocketSubscriptionResponse?
    init(url:URL){
        self.url = url
    }
    func openConnection(completion:@escaping OpenConnectionResponse){
        self.connectionDidOpen = completion
        let urlSession = URLSession.init(configuration: .default, delegate: self, delegateQueue: nil)
        //let urlSession = URLSession(configuration: .default)
        webSocketTask = urlSession.webSocketTask(with: url)
        webSocketTask?.resume()
    }
    func closeConnection(){
        webSocketTask?.cancel(with: .goingAway, reason: nil)
    }
    func subscribe(channel:Int,completion:@escaping SocketSubscriptionResponse){
        //1002
        guard let subscriptionCommands = ChannelSubscriptionRequestParam.init(command: "subscribe", channel: channel).encodeToString() else {
            return
        }
        self.subscriptionResponse = completion
        let message = URLSessionWebSocketTask.Message.string(subscriptionCommands)
        webSocketTask?.send(message) {error in
          if let error = error {
            completion(.failure(error))
          }else{
            completion(.success(true))
            }
        }
        
    }
    func receiveMessage(completion:@escaping(Result<String,Error>)->Void) {
      webSocketTask?.receive { result in
        switch result {
        case .failure(let error):
          completion(.failure(error))
        case .success(let message):
          switch message {
          case .string(let text):
            completion(.success(text))
          case .data(_):
             break
          @unknown default:
             break
          }
          self.receiveMessage(completion: completion)
        }
      }
    }
}
extension SocketWrapper: URLSessionWebSocketDelegate{
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?){
        connectionDidOpen?(true)
    }

       
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?){
        //delegate?.socketDidDisconnect()
        connectionDidOpen?(false)
    }
}
struct ChannelSubscriptionRequestParam:Codable{
    let command:String
    let channel:Int
}

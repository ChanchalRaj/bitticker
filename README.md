# BitTicker
A bitcoin ticker client app

## Getting Started

Clone the project and just run the project in XCode

### Prerequisites

Application is relying only on native iOS SDK and no third party dependency is used. Therefore, the only prerequisite is an XCode version greater or equal to 11.5

### Running
- Navigate to project directory and open project using *BitTicker.xcodeproj*
- Build and run the application on simulator or actual device running iOS 13.0 or later using Xcode 11.5 or greater.

## User Guidelines
- Sign up for the first time or use following test credentials
Test Email: raj@test.com
Test Password: 123456
- App will start fetching Bitcoins pairs data

## Technical Notes
- For Architecture purpose VIPER is used.
- App is following SOLID principle
- Keychain is used for user session persistance.
- For socket, no third-party library is used and the project relies completely on native SDK
- Tickers data is fetched via websockets by subscribing to given channel in requirement document

### Third Party
- No third party library is used


## Sample Screens
![Login View](Screenshots/Login.png)
![Register View](Screenshots/Register.png)
![Ticker View](Screenshots/Ticker.png)

## Versioning

Version 1.0
For more information on versioning, see [Semantic Versioning](http://semver.org/).

## Authors

* **Chanchal Raj** - (https://bitbucket.org/ChanchalRaj/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

